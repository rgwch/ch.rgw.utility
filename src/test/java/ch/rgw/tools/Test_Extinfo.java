package ch.rgw.tools;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import org.junit.Test;
import static org.junit.Assert.*;
import ch.rgw.io.FileTool;
import ch.rgw.tools.json.*;
import io.vertx.core.json.JsonObject;

public class Test_Extinfo {

    @Test
    public void testJson() throws Exception {
        FileInputStream fi = new FileInputStream("target/test-classes/test4.bin");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        FileTool.copyStreams(fi, baos);
        byte[] contents = baos.toByteArray();
        byte[] modified = ExtInfo.addTrace(contents, "Statusänderung", "__Test__");
        Hashtable<Object, Object> ht2 = ExtInfo.fold(modified);
        byte[] raw = (byte[]) ht2.get("Statusänderung");
        assertTrue("get ht2", raw != null);
        List<String> trace2 = StringTool.unpack(raw);
        assertTrue("Strings contents", trace2.contains("__Test__"));

    }

    @Test
    public void checkJava() throws Exception {
        FileInputStream fi = new FileInputStream("target/test-classes/test4.bin");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        FileTool.copyStreams(fi, baos);
        byte[] contents = baos.toByteArray();
        // ExtInfo exi = new ExtInfo();
        Hashtable<Object, Object> ht = ExtInfo.fold(contents);
        assertTrue("Fold error", ht != null);
        byte[] raw = (byte[]) ht.get("Statusänderung");
        assertTrue("get", raw != null);
        List<String> trace = StringTool.unpack(raw);
        trace.add("Test Entry");
        assertTrue("Strings contents", trace.contains("Test Entry"));
        byte[] packed = StringTool.pack(trace);
        ht.put("Statusänderung", packed);
        byte[] flat = ExtInfo.flatten(ht);
        assertTrue("Flatten error", flat != null);
        Hashtable<Object, Object> ht2 = ExtInfo.fold(flat);
        raw = (byte[]) ht2.get("Statusänderung");
        assertTrue("get ht2", raw != null);
        List<String> trace2 = StringTool.unpack(raw);
        assertTrue("Strings contents", trace2.contains("Test Entry"));

    }

    @Test
    public void addEntry() throws Exception {
        FileInputStream fi = new FileInputStream("target/test-classes/test4.bin");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        FileTool.copyStreams(fi, baos);
        byte[] contents = baos.toByteArray();
        byte[] modified = ExtInfo.putEntry(contents, "Testentry", "Testvalue");
        Hashtable<Object, Object> ht = ExtInfo.fold(modified);
        assertTrue(ht.get("Testentry").equals("Testvalue"));
    }
}
