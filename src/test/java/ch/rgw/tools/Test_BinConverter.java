package ch.rgw.tools;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

/**
 * Created by gerry on 23.07.16.
 */
public class Test_BinConverter {

    @Test
    public void TestInteger(){
        byte[] buf=new byte[128];
        int j=0;
        for(int i=0;i<Integer.MAX_VALUE-1000;i+=1000){
            BinConverter.intToByteArray(i,buf,j);
            Assert.assertEquals(i,BinConverter.byteArrayToInt(buf,j));
            if(++j>120){
                j=0;
            }
        }
    }

    @Test
    public void TestLong(){
        byte[] buf=new byte[128];
        int[] ibuf=new int[128];
        int j=0;
        for(long i=0;i<Long.MAX_VALUE-99999999999L;i+=99923456789L){
            BinConverter.longToByteArray(i,buf,j);
            Assert.assertEquals(i,BinConverter.byteArrayToLong(buf,j));
            BinConverter.longToIntArray(i,ibuf,j);
            Assert.assertEquals(i,BinConverter.intArrayToLong(ibuf,j));
            if(++j>120){
                j=0;
            }
        }
    }

    @Test
    public void TestLoHiInt(){
        int lo=Integer.MAX_VALUE;
        int hi=Integer.MAX_VALUE/2;
        long l=BinConverter.makeLong(lo,hi);
        Assert.assertEquals(lo,BinConverter.longLo32(l));
        Assert.assertEquals(hi,BinConverter.longHi32(l));
    }

    @Test
    public void TestHex(){
        byte[] buf=new byte[256];
        for(int i=0;i<256;i++){
            buf[i]=(byte)i;
        }
        byte[] res=Arrays.copyOf(buf,256);
        String hex=BinConverter.bytesToHexStr(buf,10,20);
        BinConverter.hexStrToBytes(hex,res,0,10,20);
        Assert.assertArrayEquals(res,buf);
        hex=BinConverter.bytesToHexStr(buf);
        byte[] cmp=new byte[256];
        BinConverter.hexStrToBytes(hex,cmp,0,0,256);
        Assert.assertArrayEquals(cmp,buf);
    }

}
