package ch.rgw.tools.crypt;

import static org.junit.Assert.*;

import org.junit.Ignore;

import ch.rgw.tools.StringTool;
import org.junit.Test;

/**
 * Created by gerry on 26.03.16.
 */
public class Test_Util {

    @Test
    @Ignore
    public void testPasswordStandardisation() {
        String pwd = "abcdef";
        byte[] check = UtilKt.standardizePassword(pwd, 8);
        assertTrue(check.length == 8);
        pwd = "abcdefgh";
        check = UtilKt.standardizePassword(pwd, 8);
        assertTrue(check.length == 8);
        pwd = "abcdefghi";
        check = UtilKt.standardizePassword(pwd, 8);
        assertTrue(check.length == 8);
        check = UtilKt.standardizePassword(pwd, 64);
        assertTrue(check.length == 64);
    }

    @Test
    @Ignore
    public void testSHA1() throws Exception {
        String input = "1234567890qwertzuiopüasdfghjklöyxcvbnmQWERTZUIOPASDFGHJKLYXCVBNM";
        byte[] sha1 = UtilKt.makeHash(input.getBytes("utf-8"));
        assertEquals(StringTool.byteArraytoHex(sha1), "046e751db641ab1a2de1150de671cb6770ed907f");
    }
}
