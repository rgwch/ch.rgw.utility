package ch.rgw.tools.crypt;

import ch.rgw.io.FileTool;

import org.junit.Ignore;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import java.util.Random;

import static org.junit.Assert.*;

/**
 * Created by gerry on 26.03.16.
 */
public class Test_Twofish {
    int[] buffersizes = { 7, 8, 9, 15, 16, 17, 31, 32, 33, 63, 64, 65, 127, 128, 129 };

    @Test
    @Ignore
    public void testTwofish() {
        assertTrue(Twofish_Algorithm.self_test());
    }

    @Test
    @Ignore
    public void testSingleBlock() throws Exception {
        Object key = Twofish_Algorithm.makeKey("topsecrt".getBytes("utf-8"));
        Object badKey = Twofish_Algorithm.makeKey("badluckk".getBytes("utf-8"));
        // first an array of 0 values
        byte[] original = new byte[Twofish_Algorithm.BLOCK_SIZE];
        byte[] encrypted = Twofish_Algorithm.blockEncrypt(original, 0, key);
        byte[] decrypted = Twofish_Algorithm.blockDecrypt(encrypted, 0, key);
        assertArrayEquals(original, decrypted);
        byte[] wrong = Twofish_Algorithm.blockDecrypt(encrypted, 0, badKey);
        assertFalse(Arrays.equals(original, wrong));
        // then an Array of arbitrary values
        Random rnd = new Random();
        rnd.nextBytes(original);
        encrypted = Twofish_Algorithm.blockEncrypt(original, 0, key);
        decrypted = Twofish_Algorithm.blockDecrypt(encrypted, 0, key);
        assertArrayEquals(original, decrypted);
        wrong = Twofish_Algorithm.blockDecrypt(encrypted, 0, badKey);
        assertFalse(Arrays.equals(original, wrong));

    }

    @Test
    @Ignore
    public void checkStreams() throws Exception {
        for (int size : buffersizes) {
            byte[] keyraw = UtilKt.standardizePassword("topsecretstrongpassword", 16);
            Object key = Twofish_Algorithm.makeKey(keyraw);
            checkStream(size, key);
        }
    }

    private void checkStream(int len, Object key) throws Exception {
        byte[] orig = UtilKt.randomArray(len);
        ByteArrayInputStream bais = new ByteArrayInputStream(orig);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        TwofishOutputStream tos = new TwofishOutputStream(baos, key);
        FileTool.copyStreams(bais, tos);
        tos.close();
        byte[] encrypted = baos.toByteArray();
        ByteArrayInputStream ebais = new ByteArrayInputStream(encrypted);
        TwofishInputStream tis = new TwofishInputStream(ebais, key);
        ByteArrayOutputStream dbaos = new ByteArrayOutputStream();
        FileTool.copyStreams(tis, dbaos);
        dbaos.close();

        byte[] decrypted = dbaos.toByteArray();
        assertArrayEquals(orig, decrypted);
    }
}
