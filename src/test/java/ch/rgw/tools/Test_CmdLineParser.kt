package ch.rgw.tools

import org.junit.Assert
import org.junit.Test

/**
 * Created by gerry on 29.04.16.
 */

class Test_CmdLineParser {

    @Test
    fun testSimpleCmdLine(){
        val cmd=CmdLineParser(switches="verbose,free",commands="run,do_it")
        Assert.assertFalse(cmd.parse("run -f --foo=bar"))
        Assert.assertTrue(cmd.parse("-f do_it --verbose=5"))
        Assert.assertEquals("do_it",cmd.command)
        Assert.assertEquals("1",cmd.get("free"))
        Assert.assertEquals("5",cmd.get("verbose"))
        Assert.assertEquals("",cmd.get("oops"))
    }

    @Test
    fun testComplexCmdLine(){
        val cmd=CmdLineParser(switches="length,width,height,black,weight", commands="build,destroy")
        Assert.assertTrue(cmd.parse("build -lwh -b=deep --weight=25"))
        Assert.assertEquals("1", cmd.get("length"))
        Assert.assertEquals("1", cmd.get("width"))
        Assert.assertEquals("1", cmd.get("height"))
        Assert.assertEquals("deep", cmd.get("black"))
        Assert.assertEquals("25", cmd.get("weight"))
        Assert.assertEquals("", cmd.get("red"))

    }
}