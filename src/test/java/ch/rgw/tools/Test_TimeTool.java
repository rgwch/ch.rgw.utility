package ch.rgw.tools;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Created by gerry on 29.10.15.
 */
public class Test_TimeTool {

  @Test
  public void checkParser(){
    TimeTool tt=new TimeTool("25.10.2015");
    assertEquals(2015, tt.get(TimeTool.YEAR));
    assertEquals(9, tt.get(TimeTool.MONTH));
    assertEquals(25, tt.get(TimeTool.DAY_OF_MONTH));

    tt=new TimeTool("2015-10-25");
    assertEquals(2015,tt.get(TimeTool.YEAR));
    assertEquals(9, tt.get(TimeTool.MONTH));
    assertEquals(25, tt.get(TimeTool.DAY_OF_MONTH));

    tt=new TimeTool("2015-10-25T07:02:44+01:00");
    assertEquals(2015,tt.get(TimeTool.YEAR));
    assertEquals(9, tt.get(TimeTool.MONTH));
    assertEquals(25, tt.get(TimeTool.DAY_OF_MONTH));


  }
}
