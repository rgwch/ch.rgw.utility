package ch.rgw.tools;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Created by gerry on 13.05.16.
 */

public class DateParserTest {
    static final String[] shortform = {
            "20160603",
            "3.6.2016",
            "03.06.2016",
            "2016-06-03",
            "2016-6-3",
            "6/3/2016",
            "06/03/2016",
            "2016/06/03",
            "2016/6/3",
            "3 Juni 2016",
            "3. Juni 2016",
            "03 Jun 2016",
            "3. Jun 2016",
            "3 jun 2016"
    };

    static final String[] longform = {
            "201606031330",
            "20160603 1330",
            "03-06-2016 13:30",
            "3.6.2016 13:30",
            "2016-06-03 13:30",
            "2016-06-03T13:30:00",
            "2016-06-03T13:30:00Z",
            "6/3/2016 13:30",
            "06/03/2016 13:30",
            "2016/06/03 13:30",
            "2016/6/3 13:30",
            "3. Juni 2016, 13:30",
            "3 Jun 2016 13:30",
            "3. Jun 2016, 13:30",
            "20160603133000",
            "20160603 133000",
            "3-6-2016 13:30:00",
            "3.6.2016 13:30:00"

    };

    @Test
    public void testShortForms() throws Exception {
        for (String ex : shortform) {
            TimeTool check = DateParser.parse(ex);
            Assert.assertEquals("20160603", check.toString(TimeTool.DATE_COMPACT));
        }
    }

    @Test
    public void testLongForms() throws Exception{
        for (String ex : longform) {
            TimeTool check = DateParser.parse(ex);
            Assert.assertEquals("03.06.2016, 13:30:00", check.toString(TimeTool.FULL));
        }
    }

}
