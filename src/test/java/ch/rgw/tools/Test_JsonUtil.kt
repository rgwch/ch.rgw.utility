package ch.rgw.tools

import ch.rgw.tools.json.*
import io.vertx.core.json.JsonArray
import org.junit.Assert
import org.junit.Test
import org.junit.Ignore
import java.io.File
import ch.rgw.tools.json.validate
import io.vertx.core.json.JsonObject
import java.util.Hashtable

/**
 * Created by gerry on 25.08.16.
 */
class Test_JsonUtil {

    @Test 
    fun testValidate(){
       val check= json_ok().put("boolean",true).put("json", json_create("test:subobject"))
        .put("array", JsonArray().add(json_create("test:InArray"))).put("int",5).put("float",1.2)
        Assert.assertTrue(check.validate("status:string","boolean:boolean","json:object","array:array","int:number","float:number"))
    }

    @Test 
    fun testParser(){
        val check= json_create("status:ok","boolean:true","integer:42")
        Assert.assertTrue(check.validate("status:string","boolean:boolean","integer:number"))
    }

    @Test 
    fun testFlush(){
        val check= JsonUtil.createFromFile(File("target/test-classes/jsonutil.conf"))
        check.put("aberhallo","jawohl")
        Assert.assertTrue(check.flush())
        val ch2=  JsonUtil.createFromFile(File("target/test-classes/jsonutil.conf"))
        Assert.assertEquals(ch2.getString("aberhallo"),"jawohl")

    }

    @Test
    fun testEncrypt(){
        val check= JsonUtil.createFromFile(File("target/test-classes/jsonutil.conf"))
        val key="must be 8, 16, 24 or 32 Chars  !"
        val encrypted=check.encrypt(key)
        val decrypted= decrypt(encrypted,key)
        Assert.assertTrue(check.map.entries == decrypted.map.entries)
        val key2="key of random size should get normalized"
        val encrypted2=check.encrypt(key2)
        val decrypted2= decrypt(encrypted2,key2)
        Assert.assertTrue(check.map.entries == decrypted2.map.entries)

    }

    @Test 
    fun testBranching(){
        val check= JsonUtil.createFromFile(File("target/test-classes/jsonutil.conf"))
        val sub=check.getBranch("SomeObject")
        val subsub=sub.getBranch("level2")
        Assert.assertEquals(subsub.getString("description"),"SubObject")
        subsub.put("check","value")
        subsub.flush()
        val check2= JsonUtil.createFromFile(File("target/test-classes/jsonutil.conf"))
        val sub2=check2.getJsonObject("SomeObject")
        val subsub2=sub2.getJsonObject("level2")
        Assert.assertEquals(subsub2.getString("check"),"value")
    }

    @Test 
    fun testExtInfo(){
        val m=java.util.Hashtable<Any,Any>()
        m.put("a","b")
        m.put("c","d")
        m.put("e",1)
        val bin=ExtInfo.flatten(m)
        val jo=JsonUtil(bin)
        Assert.assertEquals(jo.getString("a"),"b")
        Assert.assertEquals(jo.getString("c"),"d")
        Assert.assertEquals(jo.getInteger("e"),1)
    }

    @Test
    fun testToExtInfo(){
        val jo= JsonUtil(json_create("a:b","c:d").put("int",5))
        val flat=jo.toExtInfo()
        Assert.assertEquals(jo.getString("a"),"b")
        Assert.assertEquals(jo.getString("c"),"d")
        Assert.assertEquals(jo.getInteger("int"),5)
    }

    @Test
    fun testExtToJson(){
        val m=java.util.Hashtable<Any,Any>()
        m.put("a","b")
        m.put("c","d")
        m.put("e",1)
        val js=ExtInfo.toJson(m)
        val jo=JsonObject(js)
        Assert.assertEquals(jo.getString("a"),"b")
        Assert.assertEquals(jo.getString("c"),"d")
        Assert.assertEquals(jo.getInteger("e"),1)
    }
}