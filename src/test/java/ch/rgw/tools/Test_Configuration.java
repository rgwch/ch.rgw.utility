package ch.rgw.tools;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

/**
 * Created by gerry on 28.03.16.
 */
public class Test_Configuration {

    @Test
    @Ignore
    public void test_configuration() {
        Configuration cfg = new Configuration("default.ini", "user.ini");
        assertEquals(cfg.get("foo", "hum"), "biz");
        assertEquals(cfg.get("foo/bar", "hum"), "baz");
        assertEquals(cfg.get("fun", "fun"), "funny");
    }
}
