package ch.rgw.compress;

import ch.rgw.tools.crypt.UtilKt;
import ch.rgw.io.FileTool;
import org.apache.commons.compress.bzip2.CBZip2InputStream;
import org.apache.commons.compress.bzip2.CBZip2OutputStream;
import org.junit.Ignore;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import static org.junit.Assert.assertArrayEquals;

/**
 * Created by gerry on 26.03.16.
 */
public class Compressors {
    int[] buffersizes = { 7, 8, 9, 15, 16, 17, 31, 32, 33, 63, 64, 65, 127, 128, 129, 1023, 1024, 1025, 8191, 8192,
            8193 };

    @Test
    @Ignore
    public void checkGlZs() throws Exception {
        for (int size : buffersizes) {
            checkGLZ(size);
        }
    }

    private void checkGLZ(int len) throws Exception {
        byte[] orig = UtilKt.randomArray(len);
        ByteArrayInputStream bais = new ByteArrayInputStream(orig);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        GLZOutputStream tos = new GLZOutputStream(baos);
        FileTool.copyStreams(bais, tos);
        tos.close();
        byte[] compressed = baos.toByteArray();
        ByteArrayInputStream ebais = new ByteArrayInputStream(compressed);
        GLZInputStream tis = new GLZInputStream(ebais);
        ByteArrayOutputStream dbaos = new ByteArrayOutputStream();
        FileTool.copyStreams(tis, dbaos);
        dbaos.close();

        byte[] expanded = dbaos.toByteArray();
        assertArrayEquals(orig, expanded);
    }

    @Test
    @Ignore
    public void checkBZIPs() throws Exception {
        for (int size : buffersizes) {
            checkBZIP(size);
        }
    }

    private void checkBZIP(int len) throws Exception {
        byte[] orig = UtilKt.randomArray(len);
        ByteArrayInputStream bais = new ByteArrayInputStream(orig);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        CBZip2OutputStream tos = new CBZip2OutputStream(baos);
        FileTool.copyStreams(bais, tos);
        tos.close();
        byte[] compressed = baos.toByteArray();
        ByteArrayInputStream ebais = new ByteArrayInputStream(compressed);
        CBZip2InputStream tis = new CBZip2InputStream(ebais);
        ByteArrayOutputStream dbaos = new ByteArrayOutputStream();
        FileTool.copyStreams(tis, dbaos);
        dbaos.close();

        byte[] expanded = dbaos.toByteArray();
        assertArrayEquals(orig, expanded);
    }

    @Test
    @Ignore
    public void checkHuffs() throws Exception {
        for (int size : buffersizes) {
            checkHuff(size);
        }
    }

    private void checkHuff(int len) throws Exception {
        byte[] orig = UtilKt.randomArray(len);
        HuffmanTree tree = new HuffmanTree(orig);
        ByteArrayInputStream bais = new ByteArrayInputStream(orig);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        HuffmanOutputStream tos = new HuffmanOutputStream(baos, tree, 0);
        FileTool.copyStreams(bais, tos);
        tos.close();
        byte[] compressed = baos.toByteArray();
        ByteArrayInputStream ebais = new ByteArrayInputStream(compressed);
        HuffmanInputStream tis = new HuffmanInputStream(ebais);
        ByteArrayOutputStream dbaos = new ByteArrayOutputStream();
        FileTool.copyStreams(tis, dbaos);
        dbaos.close();

        byte[] expanded = dbaos.toByteArray();
        assertArrayEquals(orig, expanded);
    }
}
