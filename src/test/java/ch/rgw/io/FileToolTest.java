package ch.rgw.io;

import static org.junit.Assert.*;

import ch.rgw.tools.crypt.UtilKt;

import org.junit.Ignore;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.security.MessageDigest;
import java.util.Map;

/**
 * Created by gerry on 26.04.16.
 */
public class FileToolTest {
  String funny = "one/two/three";
  String funny1 = funny + ".docs";
  String funny2 = "/" + funny;
  String funny3 = "/" + funny1;
  String funny4 = "one\\two\\three";
  String funny5 = funny4 + ".doxc";
  String funny6 = "C:\\" + funny4;
  String funny7 = "C:\\" + funny5;
  int[] buffersizes = { 7, 8, 9, 15, 16, 17, 31, 32, 33, 63, 64, 65, 127, 128, 129, 1023, 1024, 1025, 8191, 8192,
      8193 };
  MessageDigest md;

  @Test
  @Ignore
  public void testFileCopyMethods() throws Exception {
    File file = new File("target/test-classes/random.dat");
    FileTool.writeRandomFile(file, 8193L);
    assertTrue(file.exists());
    assertTrue(file.canRead());
    assertEquals(file.length(), 8193);
    File copy = new File("target/test-classes/copy-of-random.dat");
    copy.delete();
    assertTrue(FileTool.copyFile(file, copy, FileTool.FAIL_IF_EXISTS));
    assertFalse(FileTool.copyFile(file, copy, FileTool.FAIL_IF_EXISTS));
    assertTrue(FileTool.copyFile(file, copy, FileTool.BACKUP_IF_EXISTS));
    assertTrue(FileTool.copyFile(file, copy, FileTool.REPLACE_IF_EXISTS));
  }

  @Test
  @Ignore
  public void checkStreams() throws Exception {
    md = MessageDigest.getInstance("SHA-1");
    for (int size : buffersizes) {
      checkStream(size);
    }
  }

  private void checkStream(int len) throws Exception {
    byte[] orig = UtilKt.randomArray(len);
    ByteArrayInputStream bais = new ByteArrayInputStream(orig);
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    byte[] checksum = FileTool.copyStreamsWithChecksum(bais, baos, "SHA-1");
    baos.close();
    byte[] dest = baos.toByteArray();
    assertArrayEquals(orig, dest);
    byte[] origcheck = md.digest(orig);
    assertArrayEquals(origcheck, checksum);
    File file = new File("target/test-classes/checkbuffer.dat");
    FileTool.writeFile(file, dest);
    byte[] checkread = FileTool.readFile(file);
    assertArrayEquals(orig, checkread);
    Map<String, Object> checksumread = FileTool.readFileWithChecksum(file);
    assertArrayEquals((byte[]) checksumread.get("checksum"), origcheck);
    assertArrayEquals((byte[]) checksumread.get("contents"), orig);
    assertTrue(FileTool.deleteFile(file.getAbsolutePath()));
  }

  @Test
  @Ignore
  public void testFilePathMethods() {
    assertEquals(FileTool.getFilepath(funny), "one/two");
    assertEquals(FileTool.getFilepath(funny1), "one/two");
    assertEquals(FileTool.getFilepath(funny2), "/one/two");
    assertEquals(FileTool.getFilepath(funny3), "/one/two");
    assertEquals(FileTool.getFilepath(funny4), "one/two");
    assertEquals(FileTool.getFilepath(funny5), "one/two");
    assertEquals(FileTool.getFilepath(funny6), "C:/one/two");
    assertEquals(FileTool.getFilepath(funny7), "C:/one/two");
  }

  @Test
  @Ignore
  public void testFileNameMethods() {
    assertEquals(FileTool.getFilename(funny), "three");
    assertEquals(FileTool.getFilename(funny1), "three.docs");
    assertEquals(FileTool.getFilename(funny2), "three");
    assertEquals(FileTool.getFilename(funny3), "three.docs");
    assertEquals(FileTool.getFilename(funny4), "three");
    assertEquals(FileTool.getFilename(funny5), "three.doxc");
    assertEquals(FileTool.getFilename(funny6), "three");
    assertEquals(FileTool.getFilename(funny7), "three.doxc");
  }

  @Test
  @Ignore
  public void testRemoveExtendsion() {
    assertEquals(FileTool.getNakedFilename(funny), "three");
    assertEquals(FileTool.getNakedFilename(funny1), "three");
    assertEquals(FileTool.getNakedFilename(funny2), "three");
    assertEquals(FileTool.getNakedFilename(funny3), "three");
    assertEquals(FileTool.getNakedFilename(funny4), "three");
    assertEquals(FileTool.getNakedFilename(funny5), "three");
    assertEquals(FileTool.getNakedFilename(funny6), "three");
    assertEquals(FileTool.getNakedFilename(funny7), "three");

  }

  @Test
  @Ignore
  public void testGetExtension() {
    assertEquals(FileTool.getExtension(funny), "");
    assertEquals(FileTool.getExtension(funny1), "docs");
    assertEquals(FileTool.getExtension(funny2), "");
    assertEquals(FileTool.getExtension(funny3), "docs");
    assertEquals(FileTool.getExtension(funny4), "");
    assertEquals(FileTool.getExtension(funny5), "doxc");
    assertEquals(FileTool.getExtension(funny6), "");
    assertEquals(FileTool.getExtension(funny7), "doxc");
  }

  @Test
  @Ignore
  public void testPathCorrections() {
    String funny = "one//two///three";
    String funny2 = "/" + funny;
    String funny3 = "C:\\one\\\\two\\\\\\three";
    assertEquals(FileTool.getCorrectPath(funny), "one/two/three/");
    assertEquals(FileTool.getCorrectPath(funny2), "/one/two/three/");
    assertEquals(FileTool.getCorrectPath(funny3), "C:/one/two/three/");
    assertFalse(FileTool.isRootDir(funny));
    assertTrue(FileTool.isRootDir(funny2));
    assertTrue(FileTool.isRootDir(funny3));
  }
}
