package ch.elexis.data;

import java.io.Serializable;

/**
 * Created by gerry on 20.02.17.
 */
public class Kontakt {
  public static class statL implements Comparable<statL>, Serializable {
    private static final long serialVersionUID = 10455663346456L;
    String v;
    int c;

    public statL(){}

    statL(String vv){
      v = vv;
      c = 1;
    }

    public int compareTo(statL ot){
      return ot.c - c;
    }
  }

}
