package ch.rgw.io;

import ch.rgw.tools.json.JsonUtil;

/**
 * Created by gerry on 05.09.16.
 */
public class JsonSettings extends Settings{
    JsonUtil root;

    public JsonSettings(String... files){
        root=JsonUtil.Companion.load(files);
    }

    @Override
    protected void flush_absolute() {

    }

    @Override
    public void undo() {
        root.forEach(entry ->{
            String name=entry.getKey();
            Object value=entry.getValue();
            if(value instanceof String){
                set(name,(String)value);
            }
        });
    }
}
