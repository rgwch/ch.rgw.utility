package ch.rgw.tools

import java.io.File
import java.io.FileReader
import java.io.InputStreamReader
import java.util.*

/**
 * Very simple class for configuration files.
 * Files with Properties (key=value) are searched in the classpath.
 * The user Properties are layed over the default properties
 * @param defaults: Filename of a file with default (factory-provided) Properties
 * @param user: Filename of a file with user (user-provided) Properties
 */

class Configuration(defaults: String?=null, user: String?=null) {
    val props = Properties()

    init {
        fun loadIfExists(name: String?) {
            if(name!=null) {
                var file= File(name)
                if(file.exists() && file.canRead()){
                    props.load(FileReader(file))
                }else {
                    var ins = ClassLoader.getSystemClassLoader().getResourceAsStream(name)
                    if (ins != null) {
                        props.load(InputStreamReader(ins))
                    }
                }
            }
        }
        loadIfExists(defaults)
        loadIfExists(user)

    }

    fun get(name: String, default: String? = null) = props.getProperty(name, default)
    fun set(name: String, value: String) : Configuration {
        props.setProperty(name, value)
        return this
    }
    fun set(name: String, value: Any) : Configuration{
        props.put(name,value)
        return this
    }
    fun getObject(name: String)= props.get(name)
    fun merge(other: Configuration): Configuration{
        other.props.forEach({
            props[it.key]=it.value
        })
        return this
    }
    fun merge(other: Map<String,Any>): Configuration{
        props.putAll(other)
        return this
    }

}