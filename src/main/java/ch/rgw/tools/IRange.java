package ch.rgw.tools;

public interface IRange {

  int IS_BEFORE_OTHER = 1;
  int IS_AFTER_OTHER = 2;
  int IS_INSIDE_OTHER = 3;
  int IS_AT_BEGIN_OF_OTHER = 4;
  int IS_AT_END_OF_OTHER = 5;
  int IS_OVER_OTHER = 6;
  int IS_ZERO_LENGTH = 7;

  int getLength();

  int getPos();

  void setPos(int p);

  int getEnd();

  void setEnd(int e);

  void setLen(int l);

  /**
   * Feststellen, wie dise Range in Bezug auf eine andere liegt
   *
   * @return <ul>
   * <li>IS_BEFORE-OTHER: Liegt ganz vor der anderen</li>
   * <li>IS_AFTER_OTHER: Liegt ganz nach der anderen</li>
   * <li>IS_INSIDE_OTHER: Liegt ganz innerhalb der anderen</li>
   * <li>IS_AT_BEGIN_OF_OTHER: überlappt den Anfang der anderen</li>
   * <li>IS_AT_END_OF_OTHER: überlappt das Ende der anderen</li>
   * <li>IS_OVER_OTHER: überlagert die andere ganz</li>
   * <li>IS_ZERO_LENGTH: Länge null sekunden</li>
   * </ul>
   */
  int positionTo(GenericRange other);

  /**
   * Schnitt-Range aus zwei Ranges erzeugen
   *
   * @param other die andere Range
   * @return eine neue Range, die die Überlappung enthält oder null, wenn keine überlappung
   * vorliegt
   */
  IRange overlap(GenericRange other);

}