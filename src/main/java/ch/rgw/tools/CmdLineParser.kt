package ch.rgw.tools

import java.util.*

/**
 * A command line parser vor JVM apps.
 * supports following types of command line parameters:
 * <ul>
 *     <li>* command. e.g. program do_it</li>
 *     <li>* value, e.g. program --foo=bar</li>
 *     <li>* flag, e.g. program --quiet</li>
 *  </ul>
 *  and of course all combinations thereof.
 *  Supports unix-style with single-dash short and double-dash long names
 *  e.g. program run --verbose --forever is equivalent to programm run -vf or program run -v -f
 *
 *  usage:
 *  val parser=CmdLineParser("verbose,forever",commands="do_it,run")
 * Created by gerry on 29.04.16.
 */
class CmdLineParser(val commands: String="", val switches: String=""){
    private val sw=switches.split("\\s*,\\s*".toRegex())
    private val cmd=commands.split("\\s*,\\s*".toRegex())
    val parsed=HashMap<String,String>()
    var errmsg : String=""
    var command=""

    fun get(switch: String, default: String=""): String{
        return parsed.getOrDefault(switch,default)
    }

    private fun fail(msg: String):Boolean{
        errmsg=msg
        return false
    }

    fun parse(cmdline: String) : Boolean{
        return parse(cmdline.split("\\s+".toRegex()).toTypedArray())

    }
    fun parse(args: Array<String>): Boolean{
        parsed.entries.clear()
        args.forEach { arg:String ->
            val value=arg.split("=")
            if(value[0].startsWith("--")){
                val found=sw.indexOf(value[0].substring(2))
                if(found==-1){
                    return(fail("undefined parameter ${arg}"))
                }
                parsed.put(sw[found],if(value.size==2)value[1] else "1")
            }else if(arg.startsWith("-")){
                if(value[0].length>2 && value.size>1){
                    return(fail("illegal single dash value ${arg}"))
                }
                arg.substring(1).takeWhile { it.isLetter() }.forEach { letter->
                    val found=sw.indexOfFirst { it.startsWith(letter) }
                    if(found==-1){
                        return(fail("undefined parameter ${letter} in ${arg}"))
                    }
                    parsed.put(sw[found],if(value.size==2)value[1] else "1")
                }

            }else{
                val found=cmd.indexOf(arg)
                if(found==-1){
                    return fail("undefined command ${arg}")
                }
                command=arg
            }

        }
        return true
    }
}
