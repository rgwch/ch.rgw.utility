package ch.rgw.tools.crypt

/**
 * Created by gerry on 06.09.15.
 */

fun LongToBytes(number: Long, array: ByteArray, offset: Int) {
	array[offset] = ((number ushr 56) and 0xff).toByte()
	array[offset + 1] = ((number ushr 48) and 0xff).toByte()
	array[offset + 2] = ((number ushr 40) and 0xff).toByte()
	array[offset + 3] = ((number ushr 32) and 0xff).toByte()
	array[offset + 4] = ((number ushr 24) and 0xff).toByte()
	array[offset + 5] = ((number ushr 16) and 0xff).toByte()
	array[offset + 6] = ((number ushr 8) and 0xff).toByte()
	array[offset + 7] = number.toByte()
}

fun IntToBytes(number: Int, array: ByteArray, offset: Int) {
	array[offset] = ((number ushr 24) and 0x0ff).toByte()
	array[offset + 1] = ((number ushr 16) and 0x0ff).toByte()
	array[offset + 2] = ((number ushr 8) and 0x0ff).toByte()
	array[offset + 3] = number.toByte()
}

fun BytesToInt(array: ByteArray, offset: Int) : Int{
	var ret: Long = ((array[offset].toInt() and 0xff) shl 24).toLong()
	ret = ret or (array[offset + 1].toLong() and 0xff) shl 16
	ret =ret or (array[offset + 2].toLong() and 0xff) shl 8
	ret = ret or (array[offset + 3].toLong() and 0xff)
	return ret.toInt()
}

fun BytesToLong(array: ByteArray, offset: Int): Long {
	var ret = (array[offset].toLong() and 0xff) shl 56
	ret = ret or ((array[offset + 1].toLong() and 0xff) shl 48)
	ret = ret or ((array[offset + 2].toLong() and 0xff) shl 40)
	ret = ret or ((array[offset + 3].toLong() and 0xff) shl 32)
	ret = ret or ((array[offset + 4].toLong() and 0xff) shl 24)
	ret = ret or ((array[offset + 5].toLong() and 0xff) shl 16)
	ret = ret or ((array[offset + 6].toLong() and 0xff) shl 8)
	ret = ret or ((array[offset + 7].toLong() and 0xff))
	return ret
}