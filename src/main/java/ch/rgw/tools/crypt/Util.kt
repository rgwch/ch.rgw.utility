package ch.rgw.tools.crypt

import ch.rgw.tools.BinConverter
import java.security.MessageDigest
import java.util.*
import kotlin.text.Charsets

/**
 * Created by gerry on 26.03.16.
 */
fun standardizePassword(pwd: String, len: Int): ByteArray {
    return standardizePassword(pwd.toByteArray(Charsets.UTF_8), len)
}

fun standardizePassword(pwd: ByteArray, len: Int): ByteArray {
    if (len > pwd.size) {
        val ret = ByteArray(len)
        var j = 0
        for (i in ret.indices) {
            ret[i] = pwd[j++]
            if (j >= pwd.size) {
                j = 0
            }
        }
        return ret
    } else if (len < pwd.size) {
        val ret = pwd.copyOfRange(1, len+1);
        return ret
    } else {
        return pwd;
    }
}

fun randomArray(len: Int): ByteArray{
    val rnd= Random()
    val ret=ByteArray(len)
    rnd.nextBytes(ret)
    return ret
}

fun makeHash(inp: ByteArray) : ByteArray{
    val md=MessageDigest.getInstance("SHA-1")
    return md.digest(inp)
}

fun makeHash(inp: String) : String=BinConverter.bytesToHexStr(makeHash(inp.toByteArray()))