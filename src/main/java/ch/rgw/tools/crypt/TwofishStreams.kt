/**
 * ******************************************************************
 * Copyright (c) 2014 by Gerry Weirich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ***********************************************************************
 */

package ch.rgw.tools.crypt

import java.io.FilterInputStream
import java.io.FilterOutputStream
import java.io.InputStream
import java.io.OutputStream

import ch.rgw.tools.crypt.CRC32_Adler
import ch.rgw.tools.crypt.BytesToInt
import ch.rgw.tools.crypt.IntToBytes
import ch.rgw.tools.crypt.Twofish_Algorithm
import java.util.*
import java.util.logging.Logger

/**
 * FilterOutputStream that encrypts data while writing.
 *
 * The last byte of the written file contain the number of valid bytes in the last block
 */
class TwofishOutputStream(over: OutputStream, keyobj: Any) : FilterOutputStream(over) {
    val log = Logger.getLogger("TwofishOutputStream")
    val BLOCKSIZE = Twofish_Algorithm.blockSize()
    val buffer = ByteArray(BLOCKSIZE)
    var pointer = 0
    var bytesWritten = 0L;
    val crc = CRC32_Adler()
    val son = over
    val key = keyobj
    var bDirty = false

    init {
        crc.initCRC()
    }

    /**
     * Collect the bytes to output in a buffer of Twofish-Blocksize. As the Buffer is filled, encrypt it
     * and write it to the underlying Stream
     */
    override fun write(b: Int) {
        crc.upd(b.toByte())
        buffer[pointer] = b.toByte()
        if (pointer == BLOCKSIZE - 1) {
            son.write(Twofish_Algorithm.blockEncrypt(buffer, 0, key))
            pointer = 0
        } else {
            pointer += 1
        }
        bytesWritten += 1;
        bDirty = true
    }

    override fun write(bytes: ByteArray, off: Int, len: Int) {
        for (i in 0..len - 1) {
            write(bytes[off + i].toInt())
        }
    }

    override fun write(bytes: ByteArray) {
        write(bytes, 0, bytes.size)
    }

    /**
     * Fill the buffer with random Bytes. Append a last block with meta informations
     * Write the number of valid bytes in the last byte.
     * If the Buffer has less than one Byte free, a new Block is created and appended, with the last
     * byte set to 0
     * Then, encrypt the last buffer an flush it out.
     */
    override fun flush() {
        // fill the current block with randoms. 'pointer' holds the number of valid bytes
        if (bDirty) {
            var remain = BLOCKSIZE - pointer
            log.fine("flushing after ${bytesWritten} bytes written, with ${remain} bytes")

            if (remain > 0) {
                fill(pointer, remain)
            }
            son.write(Twofish_Algorithm.blockEncrypt(buffer, 0, key))

            // create a final buffer for crc and size informations
            fill(0, BLOCKSIZE)
            IntToBytes(crc.get(), buffer, 0)
            buffer[BLOCKSIZE - 1] = (pointer - 1).toByte()
            son.write(Twofish_Algorithm.blockEncrypt(buffer, 0, key))
            son.flush();
            bDirty = false;
        }

    }

    // fill buffer from /start/ with random bytes
    private fun fill(start: Int, number: Int) {
        val rnd = Random(start.toLong())
        for (i in start..start + number - 1) {
            buffer[i] = rnd.nextInt(256).toByte()
        }
    }
}

/**
 * FilterInputStream that decrypts data as it reads them from the underlying InputStream.
 * A Checksum is computed to tell, whether the file is valid.
 */
class TwofishInputStream(from: InputStream, keyobj: Any) : FilterInputStream(from) {
    val log = Logger.getLogger("TwofishinputStream")
    val BLOCKSIZE = Twofish_Algorithm.blockSize()
    //val buffers = Array.ofDim[Array[Byte]](3)
    val buffers = arrayOf(ByteArray(BLOCKSIZE), ByteArray(BLOCKSIZE), ByteArray(BLOCKSIZE))
    val father = from
    val key = keyobj

    //val buffers = Seq(Array.ofDim[Byte](BLOCKSIZE), Array.ofDim[Byte](BLOCKSIZE), Array.ofDim[Byte](BLOCKSIZE))
    private var buffer_1 = 0
    private var buffer_2 = 1
    private var buffer_3 = 2
    private var pointer = 0
    private var EOF = Int.MAX_VALUE
    private val crc = CRC32_Adler()
    private var fileOK = true
    private var totalBytesRead: Long = 0
    private var bytesOK = 0;

    fun isOK() = fileOK && (BytesToInt(buffers[buffer_2], 0) == crc.get())

    /*
     * When reading, we must load a full Twofish block. If this was the last block of the original file,
     * probably not all bytes are valid. Unfortunately we can tell only after we read the following trailing
     * block, which contains meta informations. But the trailing block does not carry any informations to tell
     * that it is indeed the trailing block. So we must try to read yet another block (okay, one byte would be sufficient)
     * to tell, whether it was the final block of the file. Thus the strategy is:
     * - read one
      * block of data
     * - read a second block, which is probably the trailer
     * - read a third  block to tell, if it was the trailer.
     * if the third block could be read (no EOF), then the first block can be read completely, and after this,
     * the second block becomes the first and the former third becomes the second, and the former first block
     * is recycled to be the third.
     * If the third block gave EOF, the second block will tell, how many bytes of the first block are valid.
     *
     */
    val workBuffer = ByteArray(BLOCKSIZE)

    init {
        crc.initCRC()
        bytesOK = readFully(0)

        if (bytesOK < BLOCKSIZE) {
            EOF = 0 // format error: Not a single block in file
            fileOK = false
        } else {
            buffers[buffer_1] = Twofish_Algorithm.blockDecrypt(workBuffer, 0, key)
            bytesOK = readFully(0)
            if (bytesOK < BLOCKSIZE) {
                EOF = 0 // format error: No trailer in file
                fileOK = false
            } else {
                buffers[buffer_2] = Twofish_Algorithm.blockDecrypt(workBuffer, 0, key)
                peek()
            }
        }

    }

    fun peek() {
        bytesOK = readFully(0)
        if (bytesOK == -1) {
            // got trailer; read position of last valid byte
            EOF = buffers[buffer_2][BLOCKSIZE - 1].toInt()
        } else {
            // not a trailer -> continue reading
            buffers[buffer_3] = Twofish_Algorithm.blockDecrypt(workBuffer, 0, key)
        }
    }

    fun rotate() {
        buffer_1 = if (buffer_1 == 2) 0 else buffer_1 + 1
        buffer_2 = if (buffer_2 == 2) 0 else buffer_2 + 1
        buffer_3 = if (buffer_3 == 2) 0 else buffer_3 + 1
        pointer = 0
    }

    /**
     * read a whole Block. Some InputStreams do not fill the buffer in one read attempt (e.g. network streams). So we continue
     * requesting bytes, until we either have a full block or an EOF.
     */
    private fun readFully(pos: Int): Int {
        val bytesRead = father.read(workBuffer, pos, BLOCKSIZE - pos)

        if (bytesRead == -1) {
            log.fine("encountered eof at ${totalBytesRead}")
            return -1
        } else if (bytesRead < BLOCKSIZE - pos) {
            log.fine("incomplete Block: ${bytesRead} at ${totalBytesRead}")
            val nextPos = bytesRead + pos
            return readFully(nextPos)
        }

        return bytesRead
    }

    /**
     * Read return one byte from the decrypted buffer (as Int). If the buffer is empty, get the next one.
     * If end of file is reached, return -1
     */
    override fun read(): Int {
        if (pointer > EOF) {
            log.fine("read behind eof at ${totalBytesRead}")
            return -1
        } else {
            if (pointer == BLOCKSIZE) {
                rotate()
                peek()
                if (EOF == -1) {
                    log.fine("EOF was -1 after peek at ${totalBytesRead}")
                    return -1
                }
            }
            pointer += 1
            var ret: Int = buffers[buffer_1][pointer - 1].toInt() and 0xff
            crc.upd(ret.toByte())
            totalBytesRead += 1
            return ret

        }
    }

    override fun read(buffer: ByteArray, pos: Int, num: Int): Int {
        var i = 0
        do {
            var c = read()
            if (c == -1) {
                break
            }
            buffer[pos + i++] = c.toByte()
            if (i >=num) {
                break
            }
        } while (pos+i<buffer.size)
        return if (i == 0) -1 else i
    }
}