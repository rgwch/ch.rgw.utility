/*******************************************************************************
 * Copyright (c) 2005-2013, G. Weirich and Elexis
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * <p>
 * Contributors:
 * G. Weirich - initial implementation
 *******************************************************************************/

package ch.rgw.tools;

public class GenericRange implements IRange {
  int pos;
  int len;

  public GenericRange(GenericRange other) {
    pos = other.pos;
    len = other.len;
  }

  public GenericRange() {
    pos = 0;
    len = 0;
  }

  public GenericRange(int pos) {
    this.pos = pos;
    len = 0;
  }

  public GenericRange(int start, int len) {
    pos = start;
    this.len = len;
  }

  /* (non-Javadoc)
   * @see ch.rgw.tools.IRange#getLength()
   */
  public int getLength() {
    return len;
  }

  /* (non-Javadoc)
   * @see ch.rgw.tools.IRange#getPos()
   */
  public int getPos() {
    return pos;
  }

  /* (non-Javadoc)
   * @see ch.rgw.tools.IRange#setPos(int)
   */
  public void setPos(int p) {
    int end = getEnd();
    pos = p;
    setEnd(end);
  }

  /* (non-Javadoc)
   * @see ch.rgw.tools.IRange#getEnd()
   */
  public int getEnd() {
    return pos + len - 1;
  }

  /* (non-Javadoc)
   * @see ch.rgw.tools.IRange#setEnd(int)
   */
  public void setEnd(int e) {
    len = e - pos;
  }

  /* (non-Javadoc)
   * @see ch.rgw.tools.IRange#setLen(int)
   */
  public void setLen(int l) {
    len = l;
  }

  /* (non-Javadoc)
   * @see ch.rgw.tools.IRange#positionTo(ch.rgw.tools.GenericRange)
   */
  public int positionTo(GenericRange other) {
    if (len == 0 || (other.len == 0)) {
      return IS_ZERO_LENGTH;
    }
    if (pos <= other.pos) {
      if (getEnd() <= other.pos) {
        return IS_BEFORE_OTHER;
      } else if (getEnd() >= other.getEnd()) {
        return IS_OVER_OTHER;
      } else {
        return IS_AT_BEGIN_OF_OTHER;
      }
    } else // offset > other.offset)
    {
      if (getEnd() <= other.getEnd()) {
        return IS_INSIDE_OTHER;
      } else if (pos >= other.getEnd()) {
        return IS_AFTER_OTHER;
      } else {
        return IS_AT_END_OF_OTHER;
      }
    }
  }

  /* (non-Javadoc)
   * @see ch.rgw.tools.IRange#overlap(ch.rgw.tools.GenericRange)
   */
  public IRange overlap(GenericRange other) {
    IRange ret = null;
    switch (positionTo(other)) {
      case IS_BEFORE_OTHER:
      case IS_AFTER_OTHER:
      case IS_ZERO_LENGTH:
        return null;
      case IS_AT_BEGIN_OF_OTHER:
        ret = new GenericRange(pos);
        ret.setEnd(other.getEnd());
        return ret;
      case IS_OVER_OTHER:
        return other;
      case IS_INSIDE_OTHER:
        return other;
      case IS_AT_END_OF_OTHER:
        ret = new GenericRange(other.getEnd());
        ret.setEnd(getEnd());
        return ret;
    }
    return ret;
  }
}
