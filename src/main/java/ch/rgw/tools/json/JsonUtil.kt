package ch.rgw.tools.json

import ch.rgw.io.FileTool
import ch.rgw.tools.ExtInfo
import ch.rgw.tools.crypt.TwofishInputStream
import ch.rgw.tools.crypt.TwofishOutputStream
import ch.rgw.tools.crypt.Twofish_Algorithm
import io.vertx.core.json.DecodeException
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import org.apache.commons.compress.bzip2.CBZip2InputStream
import org.apache.commons.compress.bzip2.CBZip2OutputStream
import java.io.*
import java.nio.charset.Charset
import java.util.*
import java.util.logging.Logger


open class JsonUtil : JsonObject {

    private var parent: JsonUtil? = null
    private var name: String? = null
    var source: String? = null

    constructor(jo: JsonObject = JsonObject()) : super() {
        mergeIn(jo)
    }

    constructor(json: String) : super(json) {
    }

    constructor(flat: ByteArray) : super() {
        val hashTable = ExtInfo.fold(flat)
        if (hashTable != null) {
            for ((key, value) in hashTable) {
                put(key as String, value)
            }
        }
    }

    fun getBranch(name: String): JsonUtil {
        val ret = JsonUtil(super.getJsonObject(name) ?: JsonObject())
        ret.parent = this
        ret.name = name
        return ret
    }


    /**
     * get a required integer value

     * @param field field to get
     * @param min   minimum accepted value
     * @param max   maximum accepted value
     * @return the integer from the field "field"
     * @throws ParametersException if "field" was not set, or if the value was not between (not
     * *                             including) min and max.
     */
    @Throws(ParametersException::class)
    fun getInt(field: String, min: Int, max: Int): Int {
        if (!containsKey(field)) {
            throw ParametersException("field $field was not set.")
        } else {
            val `val` = getInteger(field)!!
            if (`val` < min || `val` > max) {
                throw ParametersException("field value out of range")
            } else {
                return `val`
            }
        }
    }

    /**
     * Get a String value

     * @param field   field to get
     * @param pattern regexp-Pattern the String must match
     * @param emptyok if true: missing field is acceptable
     * @return
     * @throws ParametersException if the value is not set and emptyok was false, or if the value
     * *                             was set but did nor match the pattern.
     */
    @Throws(ParametersException::class)
    fun getChecked(field: String, pattern: String, emptyok: Boolean): String {
        val raw = getString(field)
        if (raw == null || raw.length == 0) {
            if (emptyok) {
                return raw
            } else {
                throw ParametersException("field $field was not set.")
            }
        }
        if (raw.matches(pattern.toRegex())) {
            return raw
        } else {
            throw ParametersException("value of $field does not match expected criteria")
        }
    }


    /**
     * generate a String representation of the original message.body() in the
     * constructor
     */
    override fun toString(): String {
        return encodePrettily()
    }


    fun flush(): Boolean {
        if (parent != null && name != null) {
            parent!!.put(name!!, this)
            return parent!!.flush()
        } else {
            try {
                var file = File(source)
                FileTool.writeTextFile(file, toString())
                return true

            } catch(ex: Exception) {
                log.warning("could not save Json ${ex.message}")
                return false
            }

        }

    }


    fun toExtInfo() :ByteArray{
        val target = Hashtable<Any, Any>()
        target.putAll(map)
        val binary=ExtInfo.flatten(target)
        return binary
    }

    companion object {
        val log = Logger.getLogger("JsonUtil")
        // /^\d{1,2}\.\d{1,2}\.(?:\d{4}|\d{2})$/
        val ELEXISDATE = "[12][09][0-9]{6,6}"
        val NAME = "[0-9a-zA-ZäöüÄÖÜéàèß \\.-]+"
        val WORD = "[a-zA-ZäöüÄÖÜéàèß]+"
        val NOTEMPTY = ".+"
        val DATE = "[0-3]?[0-9]\\.[01]?[0-9]\\.[0-9]{2,4}"
        val PHONE = "\\+?[0-9  -/]{7,20}"
        val MAIL = ".+@[a-zA-Z_0-9\\.]*[a-zA-Z_0-9]{2,}\\.[a-zA-Z]{2,3}"
        val TIME = "[0-2][0-9]:[0-5][0-9]"
        val TEXT = "[^%\\*;:]+"
        val URL = "https?://[a-zA-Z0-9-_]+(\\.[a-zA-Z0-9-_]+)*(:[0-9]+)?"
        val IP = "[0-2]?[0-9]?[0-9]\\.[0-2]?[0-9]?[0-9]\\.[0-2]?[0-9]?[0-9]\\.[0-2]?[0-9]?[0-9]"
        val ZIP = "[A-Za-z 0-9]{4,8}"
        val UID = "[a-zA-Z0-9][a-fA-F0-9-]{8,}"
        val NUMBER = "[0-9]+"


        /**
         * Create a JsonObject from a file. // Comments are stripped before parsing

         * @param fpath filename (with relative or absolute path)
         * *
         * @return a JsonObject created from that file
         * *
         * @throws IOException     if the file was not found or could not be read
         * *
         * @throws DecodeException if the file was not a valid Json Object
         */
        @Throws(IOException::class, DecodeException::class)
        fun createFromFile(file: File, createIfNotExists: Boolean = false): JsonUtil {

            if ((!file.exists()) && (!createIfNotExists)) {
                println(file.absolutePath)
                throw IOException("File not found " + file.absolutePath)
            }
            if (file.exists()) {
                val buffer = CharArray(file.length().toInt())
                val fr = FileReader(file)
                fr.read(buffer)
                fr.close()
                val conf = String(buffer).replace("//\\s+.+\\r?\\n+\\r?".toRegex(), "")
                val result = JsonUtil(conf)
                result.source = file.absolutePath
                return result
            } else {
                val ret = JsonUtil()
                ret.source = file.absolutePath
                return ret
            }
        }

        @Throws(IOException::class, DecodeException::class)
        fun createFromStream(`is`: InputStream): JsonUtil {
            val ir = InputStreamReader(`is`)
            val sb = StringBuilder(10000)
            val buffer = CharArray(2048)
            var chars: Int
            do {
                chars = ir.read(buffer)
                sb.append(buffer, 0, chars)
            } while (chars == buffer.size)
            ir.close()
            var conf = sb.toString().replace("//\\s+.+\\r?\\n+\\r?".toRegex(), "")
            conf = conf.replace("[\\n\\r]".toRegex(), " ")
            conf = conf.replace("\\/\\*.+?\\*\\/".toRegex(), "")
            val ret = JsonUtil(conf)
            return ret

        }

        fun load(vararg names: String): JsonUtil {
            for (name in names) {
                var file = File(name)
                if (file.exists() && file.canRead()) {
                    return createFromFile(file)
                } else {
                    var url = ClassLoader.getSystemClassLoader().getResource(name)
                    if (url != null) {
                        var ret = createFromStream(url.openStream())
                        ret.source = url.toURI().toString()
                        return ret;
                    }
                }

            }
            return JsonUtil()
        }


    }
}

class ParametersException(text: String) : Exception(text) {
    companion object {
        private val serialVersionUID = 2984974711577234030L
    }
}


fun JsonObject.encrypt(passphrase: String): ByteArray {
    val charset = Charset.forName("utf-8")
    val asString = encode()
    val bais = ByteArrayInputStream(asString.toByteArray(charset))
    val baos = ByteArrayOutputStream()
    val key = Twofish_Algorithm.makeKey(passphrase.toByteArray(charset))
    val bzo = CBZip2OutputStream(baos)
    val taos = TwofishOutputStream(bzo, key)
    FileTool.copyStreams(bais, taos)
    taos.close()
    return baos.toByteArray()
}

fun decrypt(encrypted: ByteArray, passphrase: String): JsonObject {
    val charset = Charset.forName("utf-8")
    val bais = ByteArrayInputStream(encrypted)
    val cbi = CBZip2InputStream(bais)
    val key = Twofish_Algorithm.makeKey(passphrase.toByteArray(charset))
    val tais = TwofishInputStream(cbi, key)
    val baos = ByteArrayOutputStream()
    FileTool.copyStreams(tais, baos)
    val dec = String(baos.toByteArray(), charset)
    return JsonObject(dec)
}

operator fun JsonObject.get(name: String): String? {
    return map[name] as? String
}

operator fun JsonObject.set(name: String, value: String) {
    map[name] = value
}

/**
 * validate a JsonObject against a template. The template is an Array of Strings as follows:
 * name:type
 * where
 * - name is the name of the parameter,
 * - type is one of : string, boolean, number, object, array
 */
fun JsonObject.validate(vararg template: String): Boolean {
    for (field in template) {
        val (name, type) = field.split(":")
        val value = map[name]
        if (value != null) {
            when (type.toLowerCase()) {
                "string" -> if (value !is String) return false;
                "boolean" -> if (value !is Boolean) return false;
                "object" -> if (value !is JsonObject) return false;
                "array" -> if (value !is JsonArray) return false;
                "number" -> {
                    if ((value !is Double) &&
                            (value !is Float) &&
                            (value !is Int) &&
                            (value !is Long)) {
                        return false;
                    }
                }
                else -> return false;
            }

        } else {
            return false;
        }
    }
    return true;
}

/**
 * create a custom "error" response

 * @param msg an optional String describing the error. The method sends
 * *            {"status":"error","message":msg}
 */
fun JsonObject.addError(msg: String? = null): JsonObject {
    put("status", "error").put("message", msg ?: "")
    return this
}


/**
 * create a custom "ok" response

 * @param msg an optional String to add to the response. The method sends
 * *            {"status":"ok","message":msg}
 */
fun JsonObject.addOk(msg: String? = null): JsonObject {
    put("status", "ok").put("message", msg ?: "")
    return this
}

/**
 * send a custom status message

 * @param msg any String describing a status. The method sends {"status":msg}
 */
fun JsonObject.addStatus(msg: String): JsonObject {
    put("status", msg)
    return this
}

fun JsonObject.add(vararg elements: String): JsonObject {
    for (element in elements) {
        val spl = element.split(delimiters = ":", ignoreCase = false, limit = 2)
        val bool = checkBool(spl[1])
        if (bool != null) {
            put(spl[0], bool)
        } else {
            put(spl[0], checkInt(spl[1]) ?: spl[1])
        }
    }
    return this
}

fun JsonObject.replace(other: JsonObject): JsonObject {
    clear()
    mergeIn(other)
    return this
}

fun json_create(vararg elements: String): JsonObject {
    return JsonObject().add(*elements)
}


fun json_ok(): JsonObject {
    return json_create("status:ok")
}

fun json_error(message: String?): JsonObject {
    return json_create("status:error", "message:${message ?: ""}")
}

private fun checkBool(value: String): Boolean? {
    return when (value.toLowerCase()) {
        "true" -> true
        "false" -> false
        else -> null
    }
}

private fun checkInt(value: String): Int? {
    try {
        return value.toInt()
    } catch(newx: NumberFormatException) {
        return null
    }

}
