/*******************************************************************************
 * Copyright (c) 2005-2011, G. Weirich and Elexis
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * <p>
 * Contributors:
 * G. Weirich - initial implementation
 *******************************************************************************/

package ch.rgw.tools.net;

import ch.rgw.tools.crypt.BadParameterException;
import ch.rgw.tools.StringTool;

import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

/**
 * @author Gerry
 */

public class NetTool {
    public static final java.util.ArrayList<String> IPs = new java.util.ArrayList<String>();
    static final String Version = "1.0.2";
    public static String hostname;

    static {
        Enumeration<NetworkInterface> nis = null;
        try {
            nis = NetworkInterface.getNetworkInterfaces();

            while (nis.hasMoreElements()) {
                NetworkInterface ni = nis.nextElement();
                Enumeration<InetAddress> ias = ni.getInetAddresses();
                while (ias.hasMoreElements()) {
                    InetAddress ia = ias.nextElement();
                    String ip = ia.getHostAddress();
                    if (StringTool.isNothing(hostname)) {
                        hostname = ia.getHostName();
                    } else if (StringTool.isIPAddress(hostname)) {
                        if (!StringTool.isIPAddress(ia.getHostName())) {
                            hostname = ia.getHostName();
                        }
                    }
                    IPs.add(ip);
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    /**
     * Finds the matching IP from this machine's IP's
     *
     * @param net Net descritpion to check, something like 192.168.0.* or 192.168.*.*
     * @return The matching IP, like 192.168.0.5, or Null if no matching IP was found
     */
    public static String getMatchingIP(String net) throws BadParameterException {
        for (String ip : IPs) {
            if(ip.matches("[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}")) {
                if (isMatch(net, ip)) {
                    return ip;
                }
            }
        }
        return null;
    }

    public static boolean isMatch(String net, String ipv4) throws IllegalArgumentException {
        String[] src = net.split("\\.");
        String[] test = ipv4.split("\\.");
        if (src.length != 4 || test.length != 4) {
            throw (new IllegalArgumentException("Only IPv4 Adresses"));
        }
        for (int i = 0; i < 3; i++) {
            if(src[i].equals("*")){
                continue;
            }
            if(!src[i].equals(test[i])){
                return false;
            }
        }
        return true;
    }


    public static String getMacAddress(InetAddress ip) throws SocketException {
        NetworkInterface network = NetworkInterface.getByInetAddress(ip);

        byte[] mac = network.getHardwareAddress();

        System.out.print("Current MAC address : ");

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < mac.length; i++) {
            sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
        }
        return (sb.toString());
    }

    public static String getMacAddress() throws IOException {
        /* old code, windows only
        Process proc = Runtime.getRuntime().exec("cmd /c ipconfig /all");
        Scanner s = new Scanner(proc.getInputStream());
        return s.findInLine("\\p{XDigit}\\p{XDigit}(-\\p{XDigit}\\p{XDigit}){5}");
        */
        return getMacAddress(InetAddress.getLocalHost());
    }

    public static void main(String[] args) throws IOException {
        System.out.println(getMacAddress());
    }
}
