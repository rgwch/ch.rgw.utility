package ch.rgw.tools;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * Created by gerry on 20.02.17.
 */
public class ExtInfo {
  private static ObjectMapper _mapper;

  private static ObjectMapper getMapper() {
    if (_mapper == null) {
      _mapper = new ObjectMapper();
      _mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
    }
    return _mapper;
  }

  /**
   * Recreate a Hashtable from a byte array as created by flatten()
   *
   * @param flat the byte array
   * @return the original Hashtable or null if no Hashtable could be created from
   *         the array
   */
  @SuppressWarnings("unchecked")
  public static Hashtable<Object, Object> fold(final byte[] flat) throws Exception {
    Hashtable<Object, Object> res = (Hashtable<Object, Object>) foldObject(flat);
    if (res == null) {
      res = new Hashtable<Object, Object>();
    }
    return res;
  }

  /**
   * Unfold a byte array as stored by {@link #flatten(Hashtable)}
   *
   * @param flat
   * @return
   * @since 3.1
   */
  public static Object foldObject(final byte[] flat) throws Exception {
    if (flat == null || flat.length < 10) {
      return null;
    }
    try (ZipInputStream zis = new ZipInputStream(new ByteArrayInputStream(flat))) {
      ZipEntry entry = zis.getNextEntry();
      if (entry != null) {
        try (ObjectInputStream ois = new ObjectInputStream(zis)) {
          return ois.readObject();
        }
      } else {
        return null;
      }
    } catch (Exception ex) {
      System.err.println("error in foldObject: " + ex.getMessage());
      throw (ex);
    }
  }

  /**
   * Convert a Hashtable into a compressed byte array. Note: the resulting array
   * is java-specific,
   * but stable through jre Versions (serialVersionUID: 1421746759512286392L)
   *
   * @param hash the hashtable to store
   * @return
   */
  @SuppressWarnings("unchecked")
  public static byte[] flatten(Map hash) throws Exception {
    if (hash == null) {
      hash = new Hashtable<Object, Object>();
    }
    return flattenObject(hash);
  }

  /**
   * @param object
   * @return
   * @since 3.1
   */
  public static byte[] flattenObject(final Object object) throws Exception {

    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    ZipOutputStream zos = new ZipOutputStream(baos);
    zos.putNextEntry(new ZipEntry("hash"));
    ObjectOutputStream oos = new ObjectOutputStream(zos);
    oos.writeObject(object);
    zos.close();
    baos.close();
    return baos.toByteArray();

  }

  public static byte[] addTrace(final byte[] flat, final String entry, final String trace) throws Exception {
    Hashtable<Object, Object> ht = fold(flat);
    byte[] packed = (byte[]) ht.get(entry);
    List<String> unpacked;
    if (packed == null || packed.length < 5) {
      unpacked = new ArrayList<String>();
    } else {
      unpacked = StringTool.unpack(packed);
    }
    unpacked.add(trace);
    byte[] repacked = StringTool.pack(unpacked);
    ht.put(entry, repacked);
    byte[] flattened = flatten(ht);
    return flattened;
  }

  public static byte[] putEntry(final byte[] flat, final String field, final String value) throws Exception {
    Hashtable<Object, Object> ht = fold(flat);

    Object res = ht.put(field, value);
    // System.out.print(ht);
    byte[] flattened = flatten(ht);
    return flattened;
  }

  public static byte[] flattenFromJson(String json) throws Exception {
    Hashtable<Object, Object> obj = getMapper().readValue(json, Hashtable.class);
    return flatten(obj);
  }

  public static String toJson(final Hashtable<Object, Object> ht) throws Exception {
    return getMapper().writeValueAsString(ht);
  }

}
